#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stdbool.h>
/*
Opis gry w statki

Generowane są 3 staki na planszy
każdy statek zajmuje 3 pola na planszy czyli   
trzeba trafić 9 miejsc by ukończyć grę

++ - oznaczają trafienie lecz statek nie jest zatopiony 
## - oznaczają że statek został zatopiony

by ostrzelić dane polę muszą zostać wprowadzone dane
najpierw wprowadzamy cyfrę od 0 do 7 i potem bez żadnych spacji wprowadzamy WIELKĄ literę  od A do H
np.
2C (i zatwierdzamy klawiszem Enter)





             **Krótki opis** 

{
--deklaracja Struktury plansza
--funkcje
}
in main
{
    --utworzenie zmiennych typu plansza 
    --utworzenie zmiennej typu time
    { petla
        --losowanie czy dany statek ma być pionowo czy poziomo
        
        --przejscie do generowania statku pionowego 
        { if
            --losowanie pól na statek 
            { petla
                --sprawdzanie czy jest kolizja przy istniejących już statkach
            }
            --przypisanie już odpowiedniych wartosci do zmiennych typu plansza
        }
        
        --przejscie do generowania statku poziomego
        { if
            --losowanie pól na statek 
            { petla
                --sprawdzanie czy jest kolizja przy istniejących już statkach
            }
            --przypisanie już odpowiedniych wartosci do zmiennych typu plansza
        }
    }
    
    
    { petla
        --wyswietlanie planszy 
        
        --utworzenie zmiennej typu plansza 
        --wybór pola do którego chcę się dokonać strzał
        
        { petla
        	{ if
            --sprawdzanie czy takie same dane pole istnieje 
        	}
    	}

        { petla
        	{ if
            --sprawdzanie czy statek jest zatopiony
            
        	}
    	}
		--sprawdzenie czy wszystkie statki zostały zatopione
	}
}
	
	KONIEC
*/

struct plansza {
    // zmienna a i b określa miejsce pola
    int a;
    char b;
    // zmienna c - określa czy pole zostało
    // 0- nie trafione
    // 1- trafione
    // 2- zatopione
    int c;
};
	//nadanie zmiennej a i b
    void min(struct plansza *w, int c, char q) {
            w->a=c;    
            w->b=q;
    }
    //nadanie  zmiennej c
    void trafiony(struct plansza *w,int k) {
        w->c=k;
    }
    //otrzymanie wartości zmiennej c
    int egetc(struct plansza *w) {
            return w->c;
    }
    //otrzymanie wartości zmiennej a
    int egeta(struct plansza *w) {
            return w->a;
    }
    //otrzymanie wartości zmiennej b
    int egetb(struct plansza *w) {
            return w->b;
    }
    //wyswietlenie określonej zmiennej c
    void wyswietlanko(struct plansza *s){
        if(egetc(s)==0){
        	printf("  ");
        }
        if(egetc(s)==1){
        	printf("++");
        }
        if(egetc(s)==2){
        	printf("##");
        }
    }


    
    
int main(){
    //utworzenie zmiennych typu plansza i nadanie wartości 0 
    int liczba_Stat;
    struct plansza gracz[9];
    for(liczba_Stat=0;liczba_Stat<9;liczba_Stat++)
    {
    	trafiony(&gracz[liczba_Stat],0);
    }
    
    bool kolizja = false;
    int zarodek,k;
    time_t tt;
    zarodek = time(&tt);
    srand(time(&tt)*64276);  
    int i,j;
        
    for(i=0;i<8;i=i+3) {
        int pozycja_k;
        pozycja_k=(int)(rand() / (RAND_MAX + 1.0) * 2.0)+1;
        char cyfra;
        int litera;
        int cyfra1=0;
        kolizja=false;
        
        if(pozycja_k==1) {
            while(kolizja==false) {
	            cyfra1=(int)(rand() / (RAND_MAX + 1.0) * 8.0);
	            litera =  (int)((rand() / (RAND_MAX + 1.0) * 8.0))+65;
	            kolizja=true;
	            
		            for( j=0;j<9;j++) {
		                if(egeta(&gracz[j])==cyfra1 && egetb(&gracz[j])==litera) {
		                    kolizja = false;
		                    break;
		                }
		                if(egeta(&gracz[j])==(cyfra1-1) && egetb(&gracz[j])==litera) {
		                    kolizja = false;
		                    break;
		                }
		                if(egeta(&gracz[j])==(cyfra1+1) && egetb(&gracz[j])==litera) {
		                    kolizja = false;
		                    break;
		           		}
		            }
            }
	        min(&gracz[i],cyfra1,litera);
	        if(cyfra1==0){
	            min(&gracz[i+1],7,litera);
	        }
	        else 
	        {
	            min(&gracz[i+1],cyfra1-1,litera);
	        }
	        
	        if(cyfra1==7){
	            min(&gracz[i+2],0,litera);
	        }
	        else
	        {
	            min(&gracz[i+2],cyfra1+1,litera);
	        }
        }
        
        
        if(pozycja_k==2) {
            
            while(kolizja==false) {
                cyfra1=(int)(rand() / (RAND_MAX + 1.0) * 8.0);
                litera = (char)((rand() / (RAND_MAX + 1.0) * 8.0))+65;
            	kolizja=true;
            
		            for(j=0;j<9;j++) {
		                if(egeta(&gracz[j])==cyfra1 && egetb(&gracz[j])==litera) {
		                    kolizja = false;
		                    break;
		                }
		                if(egeta(&gracz[j])==cyfra1 && egetb(&gracz[j])==(char)(litera-1)) {
		                    kolizja = false;
		                    break;
		                }
		                if(egeta(&gracz[j])==cyfra1 && egetb(&gracz[j])==(char)(litera+1)) {
		                    kolizja = false;
		                    break;
		                }
		            }
        	}
        	
	        min(&gracz[i],cyfra1,litera);
	        if(litera=='A'){
	        	min(&gracz[i+1],cyfra1,'H');
	        }
	        else
	        {
	            min(&gracz[i+1],cyfra1,(char)(litera-1));
	        }
	
	        if(litera=='H'){
	       	 min(&gracz[i+2],cyfra1,'A');
	        }
	        else{
	            min(&gracz[i+2],cyfra1,(char)(litera+1));
	        }
        }
    }

    int wygranko=0;
        
    for(;;) {
        printf("\n");
        printf("  A B C D E F G H\n");
        int znak=0;
        for( k=0;k<8 ;k++){
            printf("%d|",k);
            for(i=0;i<8 ;i++){
                for( j=0;j<9;j++){
                    if(egeta(&gracz[j])==0+k && egetb(&gracz[j])=='A'+i) {
                        wyswietlanko(&gracz[j]);
                        znak=1;
                    } 
                }
                if(znak==0){
                    printf("  ");
                }
                znak=0;
            }
            printf("\n");  
        }
        
        struct plansza  szukanko;
        char cyfra;
        char litera;
        int cyfra1; 
        scanf("%d",&cyfra1);
        scanf("%c",&litera);
        int we;
        min(&szukanko,cyfra1, litera);
        for( we=0;we<9;we++) {
            if(egeta(&szukanko)==egeta(&gracz[we]) && egetb(&szukanko)==egetb(&gracz[we])) {
                printf("Trafiony");
                trafiony(&gracz[we],1);    
            }
        }
        //wykrywanie czy statek jest zatopiony
        int ll;
        for(ll=0;ll<9;ll=ll+3) {
            if(egetc(&gracz[0+ll])==1 && egetc(&gracz[1+ll])==1 && egetc(&gracz[2+ll])==1) {
            	trafiony(&gracz[0+ll],2);    
            	trafiony(&gracz[1+ll],2);    
            	trafiony(&gracz[2+ll],2);    
            	printf("Zatopiony");
            	wygranko++;
            }
        }
        if(wygranko==3){
        	printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n    Gratulacje \n    Koniec Gry");
            break;
        }
    	printf("Zatopione Statki %d/3",wygranko);
    }
    return 0;
}